class Aron {

    // Creamos un constructor 
    constructor(nombre){
        this.nombre = nombre;
    }
    // Hacemos el impresion del constuctor 
    getnombre() {
        console.log(this.nombre);
    }
    // Hacemos la funcion 
    Hacer (como_hacelo){
        como_hacelo();
    }

}
// Creamos los subfunciones 
class Cocinar {
    // le damos la Implemencacion 
    Hacer(){
        console.log("Aron está Cocinando");
    }
}

class Lavar {
    // le damos la Implemencacion
    Hacer(){
        console.log("Aron esta Lavando");
    }
}

class Estudiar {

    Estudiando(){
        console.log("Aron esta estudiando");
    }
}
// Hacemos le llamado de cada objeto
var aron = new Aron("Aron");
aron.getnombre();
var cocinar = new Cocinar();
var lavar = new Lavar();
var estudiar = new Estudiar();

// hacemos la implemencacion 
aron.Hacer(cocinar.Hacer);
aron.Hacer(lavar.Hacer);
aron.Hacer(estudiar.Estudiando);
